require 'net/http'
require 'json'

module RegionRake
  # Для того чтобы получить список регионов: http://vk.com/dev/database.getRegions

  class Region
    attr_accessor :id, :cities

    # распараллелить?
    def initialize(region_id = 1052052)
      @id = region_id
      start_time = Time.now
      puts "start time: #{start_time}"
      @cities = []
      cities_array = get_cities(region_id)["items"]
      cities_array.each do |c|
        city = City.new(c["id"], c["title"], c["area"], c["region"])
        puts city.to_s
        schools_array = get_schools(city.id)["items"]
        schools_array.each do |s|
          city.schools << School.new(s["id"], s["title"])
          puts "\t#{city.schools.last.to_s}"
        end
        @cities << city
      end
      finish_time = Time.now
      puts "finish time: #{finish_time}"
      diff_time = finish_time - start_time
      puts "elapsed time: #{diff_time} sec"
    end

    class City
      attr_accessor :id, :title, :area, :region, :schools

      def initialize(id, title, area, region,  schools = [])
        @id = id
        @title = title
        @area = area
        @region = region
        @schools = schools
      end

      def to_s
        {id: @id, title: @title, area: @area, region: @region, schools: @schools}.to_s
      end
    end

    class School
      attr_accessor :id, :title

      def initialize(id, title)
        @id = id
        @title = title
      end

      def to_s
        {id: @id, title: @title}.to_s
      end
    end

    def get_cities(region_id = 1052052) # Республика Мордовия
      uri = 'https://api.vk.com/method/database.getCities'
      params = { country_id: 1, region_id: region_id, need_all: 1, count: 1000, v: 5.37, offset: 0 }
      response = get_response(uri, params)
    end

    def get_schools(city_id = 124) # Саранск
      uri = 'https://api.vk.com/method/database.getSchools'
      params = { city_id: city_id, count: 1000, v: 5.37, offset: 0 }
      response = get_response(uri, params)
    end

    def get_response(uri, params, items = [])
      uri = URI(uri.to_s)
      uri.query = URI.encode_www_form(params)
      response = JSON.parse(Net::HTTP.get_response(uri).body)["response"]
      if !response.nil?
        count = response["count"]
        array = response["items"]
        unless items.empty?
          array = array + items
          response["items"] = array
        end
        if array.length == count
          return response
        else
          if array.length > 0
            params[:offset] = array.length
            return get_response(uri, params, array)
          else
            raise "cannot get response"
          end
        end
      else
        raise "cannot get response"
      end
      response
    end

  end
end
