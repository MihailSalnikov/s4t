require_relative 'region_rake'

namespace :schools do
  desc "TODO"
  task get_mordovia: :environment do
    mordovia = RegionRake::Region.new

    vk_region_id = mordovia.id
    region = Region.create(name: "Мордовия", vk_region_id: vk_region_id)
    cities = mordovia.cities # []
    cities.each do |c|
      vk_city_id = c.id
      name = c.title
      area = c.area
      city = City.create(name: name, area: area, vk_city_id: vk_city_id, vk_region_id: vk_region_id, region_id: region.id)
      schools = c.schools
      schools.each do |s|
        vk_school_id = s.id
        name = s.title
        School.create(name: name, vk_school_id: vk_school_id, vk_city_id: vk_city_id, city_id: city.id)
      end
    end
  end

end
