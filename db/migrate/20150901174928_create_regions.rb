class CreateRegions < ActiveRecord::Migration
  def change
    create_table :regions do |t|
      t.string :name
      t.integer :vk_region_id

      t.timestamps null: false
    end

    add_index :regions, :vk_region_id, unique: true
    add_index :regions, :name
  end
end
