class RetypeSchoolCityColumn < ActiveRecord::Migration
  def change
    remove_column :schools, :city
    add_reference :schools, :city, index: true
  end
end
