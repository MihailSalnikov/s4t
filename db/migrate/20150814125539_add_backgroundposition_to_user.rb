class AddBackgroundpositionToUser < ActiveRecord::Migration
  def change
    add_column :users, :background_fixed, :integer, in: -1..1, default: -1
  end
end
