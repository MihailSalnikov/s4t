class AddInfoToUser < ActiveRecord::Migration
  def change
    add_column :users, :activities, :text
    add_column :users, :interests, :text
    add_column :users, :music, :text
    add_column :users, :films, :text
    add_column :users, :books, :text
    add_column :users, :quotations, :text
  end
end
