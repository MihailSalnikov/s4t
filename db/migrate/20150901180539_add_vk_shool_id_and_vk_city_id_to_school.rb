class AddVkShoolIdAndVkCityIdToSchool < ActiveRecord::Migration
  def change
    add_column :schools, :vk_school_id, :integer
    add_column :schools, :vk_city_id, :integer

    add_index :schools, :vk_school_id
    add_index :schools, :vk_city_id
  end
end
