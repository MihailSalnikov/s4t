class AddMiddleNameToUser < ActiveRecord::Migration
  def change
    add_column :users, :middle_name, :string, default: ""
  end
end
