class CreateUserRoleLinks < ActiveRecord::Migration
  def change
    create_table :user_role_links do |t|
      t.integer :user_id
      t.integer :user_role_id

      t.timestamps null: false
    end
  end
end
