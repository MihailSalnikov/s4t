class AddBackgroundattachmentToUser < ActiveRecord::Migration
  def change
    add_column :users, :background_attachment, :boolean, default: false
  end
end
