class AddBackgroundToUser < ActiveRecord::Migration
  def change
    add_column :users, :background_color, :string, default: "#F5F8FA"
  end
end
