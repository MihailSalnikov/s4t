class AddAreaAndVkCityIdAndVkRegionIdAndRegionIdToCity < ActiveRecord::Migration
  def change
    add_column :cities, :area, :string
    add_column :cities, :vk_city_id, :integer
    add_column :cities, :vk_region_id, :integer
    add_column :cities, :region_id, :integer

    add_index :cities, :vk_city_id
    add_index :cities, :vk_region_id
    add_index :cities, :region_id
  end
end
