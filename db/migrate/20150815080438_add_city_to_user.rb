class AddCityToUser < ActiveRecord::Migration
  def change
    add_reference :users, :city, index: true, default: 1
  end
end
