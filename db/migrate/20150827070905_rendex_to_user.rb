class RendexToUser < ActiveRecord::Migration
  def change
    remove_index :users, :first_name
    remove_index :users, :last_name

    add_index :users, :first_name
    add_index :users, :last_name
  end
end
