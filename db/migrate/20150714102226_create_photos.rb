class CreatePhotos < ActiveRecord::Migration
  def change
    create_table :photos do |t|
      t.text :description
      t.belongs_to :user, null: false

      t.timestamps null: false
    end
  end
end
