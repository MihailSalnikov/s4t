# Be sure to restart your server when you modify this file.

# Rails.application.config.session_store :cookie_store, key: '_s4t_session'
S4t::Application.config.session_store :cookie_store, key: '_s4t_subdomains_session', domain: :all, tld_length: 2
