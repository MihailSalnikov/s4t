class SubdomainConstraint
  def self.matches?(request)
    subdomains = User::BANNED_USERNAMES
    request.subdomain.present? && !subdomains.include?(request.subdomain)
  end
end

Rails.application.routes.draw do
  constraints subdomain: false do
    root 'static#index'
    get '/about', to: 'static#about'
    get '/terms', to: 'static#terms'
    get '/catalogs', to: 'catalog#index'
    get '/catalogs/:id', to: 'catalog#users_by_school', as: "catalogs_by_school"
    ############
    get '/lenta', to: 'admin/posts#index', as: "lenta"
    post '/lenta', to: 'admin/posts#create'
    get '/lentas/:id', to: 'admin/posts#show', as: "lenta_post"
    patch '/lentas/:id', to: 'admin/posts#update'
    delete '/lentas/:id', to: 'admin/posts#destroy'
    get '/lentas/:id/edit', to: 'admin/posts#edit', as: "edit_lenta_post"
    get '/lenta/new', to: 'admin/posts#new', as: "new_lenta_post"
  end

  constraints subdomain: false do
    devise_for :users, :skip => [:sessions], :controllers => { :registrations => :registrations }
    as :user do
      get 'users/sign_in' => 'devise/sessions#new', :as => :new_user_session
      post 'users/sign_in' => 'devise/sessions#create', :as => :user_session
    end
    delete '/users_avatar', to: 'profile#delete_avatar', as: "delete_avatar"
    delete 'users_background_image', to: "profile#delete_background_image", as: "delete_background_image"
  end
  devise_scope :user do
    delete "sign_out", to: "devise/sessions#destroy"
  end

  constraints SubdomainConstraint do
    get '/', to: 'profile#index', as: "root_user"
    resources :photos
    resources :posts
    end
end
