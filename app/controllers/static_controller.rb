class StaticController < ApplicationController
  layout "sub", :only => [:index, :about]

  def index
    redirect_to root_user_url(subdomain: current_user.username) if user_signed_in?
  end

  def about
  end

  def terms
  end
end
