class ApplicationController < ActionController::Base
  include ApplicationHelper
  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :set_user, if: :subdomain_is_present?
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception


  protected
    def configure_permitted_parameters
      devise_parameter_sanitizer.for(:sign_up) << [:username, :phone, :city_id, :school_id, :first_name, :last_name, :middle_name]
      devise_parameter_sanitizer.for(:account_update) << [:about, :phone, :school_id, :city_id, :first_name, :last_name, :middle_name, :background_color, :background_image, :background_fixed, :background_attachment, :avatar, :crop_x, :crop_y, :crop_w, :crop_h, :activities, :interests, :music, :films, :books, :quotations]
    end

    def after_sign_in_path_for(resource)
      request.env['omniauth.origin'] || stored_location_for(resource) || root_user_url(subdomain: current_user.username)
    end


    def set_user
      @user = User.find_by_username(request.subdomain)
    end
end
