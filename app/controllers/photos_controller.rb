class PhotosController < ApplicationController
  before_filter :user_signed_in?, except: [:index, :show]

  include ApplicationHelper

  def new
    @photo = Photo.new
  end

  def index
    @user = User.find_by_username(request.subdomain)
    @photos = @user.photos.order(created_at: :desc)
    @photo = Photo.new
  end

  def show
    @photo = Photo.find(params[:id])
  end

  def create
    @photo = current_user.photos.build(photo_params)
    if @photo.save
      redirect_to photos_path
    else
      redirect_to new_photo_path, alert: "error"
    end
  end

  def destroy
    photo = Photo.find(params[:id])
    if photo.user == current_user
      photo.destroy
      redirect_to photos_path
    else
      redirect_to photo, alert: "У Вас недостаточно прав, авторизуйтесь или зарегистрируйтесь"
    end
  end


private

  def photo_params
    params.require(:photo).permit(:photo, :description, :dimensions)
  end



end
