module PostMethods
  extend ActiveSupport::Concern

  def index
    @posts = Post.paginate(page: params[:page], per_page: 15).order(created_at: :desc)
    respond_to do |format|
      format.html
      format.js
    end
  end


  # GET /posts/1
  # GET /posts/1.json
  def show
  end

  # GET /posts/new
  def new
    @post = Post.new
  end

  # GET /posts/1/edit
  def edit
  end
end
