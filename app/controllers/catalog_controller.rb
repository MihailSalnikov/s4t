class CatalogController < ApplicationController
  def index
    @citys = City.more_zero_schools
    @users = User.order(created_at: :desc).limit(25)
  end

  def users_by_school
    @school = School.find(params[:id])
    @users = @school.users.sort_by {|u| -u.calculate_the_rating }
  end
end
