class Admin::PostsController < ApplicationController
  include PostMethods

  before_action :tenant_switch_public
  before_action :set_post, only: [:show, :edit, :update, :destroy]
  before_action :user_have_access?, except: [:index, :show]

  layout "sub"

  # POST /posts
  # POST /posts.json
  def create
    @post = current_user.posts.build(post_params)
    respond_to do |format|
      if @post.save
        format.html { redirect_to lenta_post_path(@post), notice: 'Post was successfully created.' }
        format.json { render :show, status: :created, location: @post }
      else
        format.html { render :new }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /posts/1
  # PATCH/PUT /posts/1.json
  def update
    respond_to do |format|
      if @post.update(post_params)
        format.html { redirect_to lenta_post_path(@post), notice: 'Post was successfully updated.' }
        format.json { render :show, status: :ok, location: @post }
      else
        format.html { render :edit }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /posts/1
  # DELETE /posts/1.json
  def destroy
    @post.destroy
    respond_to do |format|
      format.html { redirect_to lenta_url, notice: 'Post was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_post
      @post = Post.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def post_params
      params.require(:post).permit(:title, :body)
    end

    def user_have_access?
      if user_signed_in? && current_user.admin?
        true
      else
        redirect_to lenta_path, alert: "У Вас недостаточно прав, авторизуйтесь или зарегистрируйтесь"
      end
    end

    def tenant_switch_public
      logger.info("!!!Apartment current tenant is #{Apartment::Tenant.current}!!!")
      Apartment::Tenant.switch!(Apartment::Tenant.default_tenant)
      logger.info("!!!Apartment switch tenant to #{Apartment::Tenant.current}!!!")
    end
end
