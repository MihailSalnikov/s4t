class ProfileController < ApplicationController
  before_filter :user_signed_in?, only: [:delete_avatar, :delete_background_image]
  before_action :tenant_switch_user, except: [:delete_avatar, :delete_background_image]

  def index
    @posts = Post.all.order(created_at: :desc).limit(5)
    @photos = @user.photos.order(created_at: :desc).limit(6)
  end


  def delete_avatar
    user = current_user
    user.avatar = nil
    user.save
    redirect_to edit_user_registration_url(subdomain: false)
  end

  def delete_background_image
    user = current_user
    user.background_image = nil
    user.save
    redirect_to edit_user_registration_url(subdomain: false)
  end

  private
    def tenant_switch_user
      @user = User.find_by_username(request.subdomain)
      Apartment::Tenant.switch!(@user.username)
    end

end
