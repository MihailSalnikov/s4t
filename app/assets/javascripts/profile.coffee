# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
proverka = (input) ->
  value = input.value
  rep = /[-\.0-9a-zA-Z]/
  if rep.test(value)
    value = value.replace(rep, '')
    input.value = value
  return
