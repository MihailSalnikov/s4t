# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$ ->
  $('.embed-video').YouTubeModal({clickOutsideClose: 1, showBorder: false, hideTitleBar: true, theme: 'dark'})
