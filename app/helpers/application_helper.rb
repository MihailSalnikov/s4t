module ApplicationHelper

  def has_user_access?
    user_signed_in? && current_user == User.find_by_username(request.subdomain)
  end

  def subdomain_is_present?
    !request.subdomain.empty?
  end

  def avatar_url(user)
    gravatar_id = Digest::MD5.hexdigest(user.email.downcase)
    "http://gravatar.com/avatar/#{gravatar_id}.png"
  end
end
