class Photo < ActiveRecord::Base
  belongs_to :user
  before_save :extract_dimensions
  serialize :dimensions

  validates :photo, presence: true
  has_attached_file :photo, :styles => { small: "300x300>", normal: "800x800>", mini: "105x105>" }
  validates_attachment_content_type :photo, :content_type => /\Aimage\/.*\z/

  private

  def extract_dimensions
    # return unless photo?
    tempfile = photo.queued_for_write[:original]
    unless tempfile.nil?
      geometry = Paperclip::Geometry.from_file(tempfile)
      self.dimensions = [geometry.width.to_i, geometry.height.to_i]
    end
  end

end
