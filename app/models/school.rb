class School < ActiveRecord::Base
  has_many :users
  belongs_to :city

  validates :name, presence: true
end
