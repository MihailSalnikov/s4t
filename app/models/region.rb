class Region < ActiveRecord::Base
  # TODO: Написать все валидации, т.к. тут нифига не все.

  validates :name, presence: true
  validates :vk_region_id, presence: true, uniqueness: true

  has_many :cities
end
