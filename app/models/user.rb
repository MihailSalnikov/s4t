class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  after_create :create_tenant, :add_role
  before_create :username_downcase
  after_destroy :destroy_tenant


  has_many :photos, dependent: :destroy
  has_many :posts, dependent: :destroy
  has_one :user_role_link
  belongs_to :school
  belongs_to :city

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable


  BANNED_USERNAMES = [ "www", "admin", "administrator", "hostmaster", "mailer-daemon",
    "postmaster", "root", "security", "support", "webmaster", "moderator",
    "moderators", "help", "contact", "fraud", "guest", "nobody", "job", "hire" ]


  # validates :email, uniqueness: {scope: :sports_org_id, allow_blank: false}
  validates :username, :school, presence: true
  validates_each :username do |record,attr,value|
    if BANNED_USERNAMES.include?(value.to_s.downcase)
      record.errors.add(attr, "is not permitted")
    end
  end
  validates :username, :format => { :with => /\A[A-Za-z0-9][A-Za-z0-9_-]{0,24}\z/ }, :uniqueness => { :case_sensitive => false }
  validates :city_id, presence: true
  validates :first_name, :last_name, presence: true
  has_attached_file :background_image
  validates_attachment_content_type :background_image, :content_type => /\Aimage\/.*\z/
  validates_format_of :background_color, with: /\A#?(?:[A-F0-9]{3}){1,2}\z/i, unless: :background_color?

  has_attached_file :avatar, :styles => { :profile => "320x320#", :small => "150x150#", :large => "500x500>" }, :processors => [:cropper], :default_url => "guest.png"
  attr_accessor :crop_x, :crop_y, :crop_w, :crop_h
  after_update :reprocess_avatar, :if => :cropping?

  def cropping?
    !crop_x.blank? && !crop_y.blank? && !crop_w.blank? && !crop_h.blank?
  end

  def avatar_geometry(style = :original)
    @geometry ||= {}
    @geometry[style] ||= Paperclip::Geometry.from_file(avatar.path(style))
  end

  def calculate_the_rating
    Apartment::Tenant.switch!(self.username)
    rating = self.posts.count / 10.0
    rating += 0.2 unless self.about.blank?
    rating += 0.2 unless self.activities.blank?
    rating += 0.2 unless self.interests.blank?
    rating += 0.2 unless self.music.blank?
    rating += 0.2 unless self.films.blank?
    rating += 0.2 unless self.books.blank?
    rating += 0.2 unless self.quotations.blank?
    return rating
  end


  def role
    self.user_role_link.user_role
  end

  def admin?
    self.role == UserRole.find_by_name("admin")
  rescue
    false
  end

  private

    def create_tenant
      Apartment::Tenant.create(username)
    end

    def username_downcase
      username.downcase!
    end

    def destroy_tenant
      Apartment::Tenant.drop(username)
    end

    def reprocess_avatar
      avatar.assign(avatar)
      avatar.save
    end

    def add_role
      user_role_link = UserRoleLink.new(user: self, user_role: (UserRole.find_by_name "user"))
      user_role_link.save
    end

end
