class City < ActiveRecord::Base
  has_many :schools
  has_many :users

  belongs_to :region

  def full_name
    full_name = self.name
    full_name += ", #{self.area}" unless self.area.blank?
    full_name += ", #{self.region.name}" unless self.region.nil?
    full_name
  end

  def self.more_zero_schools
    limit_first = 5
    # self.find_by_sql("SELECT \"public\".\"cities\".*, COUNT(\"public\".\"schools\".id) as school_count FROM \"public\".\"cities\" INNER JOIN \"public\".\"schools\" ON \"public\".\"cities\".id = \"public\".\"schools\".city_id GROUP BY \"public\".\"cities\".id ORDER BY school_count DESC")
    result = self.includes(:region, :schools).joins("INNER JOIN (SELECT \"public\".\"schools\".\"city_id\", count(\"public\".\"schools\".\"id\") AS counted_id FROM \"public\".\"schools\" GROUP BY \"public\".\"schools\".\"city_id\") AS t2 ON \"public\".\"cities\".\"id\" = \"t2\".\"city_id\"").order("COALESCE(counted_id, 0) DESC").limit(limit_first)
    first_ids = result.map(&:id).join(",")
    result += self.includes(:region, :schools).joins("INNER JOIN (SELECT \"public\".\"schools\".\"city_id\", count(\"public\".\"schools\".\"id\") AS counted_id FROM \"public\".\"schools\" GROUP BY \"public\".\"schools\".\"city_id\") AS t2 ON \"public\".\"cities\".\"id\" = \"t2\".\"city_id\" AND \"public\".\"cities\".\"id\" NOT IN (#{first_ids})").order("\"public\".\"cities\".\"name\", COALESCE(counted_id, 0) DESC")
  end
end
