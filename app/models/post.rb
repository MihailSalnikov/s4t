class Post < ActiveRecord::Base
  belongs_to :user
  validates :title, :body, :user, presence: true

  def previous
    self.class.where("id > ?", id).first
  end

  def next
    self.class.where("id < ?", id).last
  end
end
