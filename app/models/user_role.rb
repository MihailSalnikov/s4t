class UserRole < ActiveRecord::Base
  has_one :user_role_link
  validates :name, presence: true;

end
