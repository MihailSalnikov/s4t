require 'rails_helper'

RSpec.describe ProfileController, :type => :controller do
  describe "GET #index" do
      before(:each) do
        user = User.create(email: "email1@example.com", password: "password123", password_confirmation: "password123", school_id: 1, first_name: "Carl", last_name: "Super", username: "users")
        @request.host = "#{user.username}.local.dev"
      end
      it "responds successfully with an HTTP 200 status code" do
        get :index
        expect(response).to be_success
        expect(response).to have_http_status(200)
      end

      it "renders the index template" do
        get :index
        expect(response).to render_template("index")
      end
    end
end
