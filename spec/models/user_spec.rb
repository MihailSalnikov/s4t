require 'rails_helper'

RSpec.describe User, type: :model do

  describe "validates tests" do
    subject { @user }
    before do
      @user = User.new(email: "email1@example.com", password: "password123", password_confirmation: "password123", school_id: 1, first_name: "Carl", last_name: "Super")
    end

    it "valid username" do
        @user.username = "user111"
        should be_valid
    end

    it "when username is not present" do
        @user.username = ""
        should_not be_valid
    end

    # BANNED_USERNAMES = [ "www", "admin", "administrator", "hostmaster", "mailer-daemon",
    #   "postmaster", "root", "security", "support", "webmaster", "moderator",
    #   "moderators", "help", "contact", "fraud", "guest", "nobody", "job", "hire"]

    # BANNED_USERNAMES.each do |un|
    #   it "when username is #{un}" do
    #     @user.username = un
    #     should_not be_valid
    #   end
    # end

    it "validate username" do
      ["www", "admin", "administrator", "hostmaster", "mailer-daemon",
        "postmaster", "root", "security", "support", "webmaster", "moderator",
        "moderators", "help", "contact", "fraud", "guest", "nobody", "job", "hire", "lool\n"].each do |un|
          @user.username = un
          should_not be_valid
      end
    end

    it "when we create two user with one username" do
      @user.username = "username"
      @user2 = User.new(email: "email2@example.com", password: "password123", password_confirmation: "password123", username: @user.username, school_id: 1, first_name: "Carl", last_name: "Super")
      @user.save!
      expect(@user2).to be_invalid
    end

    it "when we create two user with one username, but they is different case" do
      @user.username = "UsErNamE"
      @user2 = User.new(email: "email2@example.com", password: "password123", password_confirmation: "password123", username: @user.username.downcase, school_id: 1, first_name: "Carl", last_name: "Super")
      @user.save!
      expect(@user2).to be_invalid
    end
  end

end
